#!/usr/bin/env python3
from argparse import ArgumentParser
from logging import basicConfig, debug, info, CRITICAL
from math import ceil
from random import random

from warnings import simplefilter
# Tensorflow clutters things up with deprication warnings
simplefilter(action='ignore', category=FutureWarning)

try:
    from scapy.all import rdpcap, Ether
    from matplotlib import pyplot as plt
    from matplotlib.colors import LogNorm
    from numpy import arange, asarray, mgrid, ones, unique, reshape
    from numpy.random import permutation
    from pylab import colorbar, pcolor
    from sklearn.preprocessing import MinMaxScaler
    from tensorflow import add, compat, float32, math, keras,\
                           matmul, nn, reduce_mean, transpose,\
                           zeros_initializer,\
                           Variable
except ImportError as e:
    print(("Error: {}; try: sudo apt install -y python3-pip python3-tk; "
           "pip3 install scapy sklearn matplotlib numpy tensorflow").format(str(e)))
    from sys import exit; exit(1)

FORMAT = '%(asctime)-15s %(levelname)s %(message)s'
OUTPUT_LABELS = ["http", "dns", "ftp"]

def set_args():
    """
    Set up the common arguments for all our programs

    :return: Argument parser with the common arguments
    :rtype: `py:ArgumentParser`
    """
    parser = ArgumentParser(description="Process some packets")
    parser.add_argument("pcap", type=str, nargs="+",
            help="pcap file(s) to analyze")
    parser.add_argument('--max-duration', '-m', type=float, default=2.0,
            help="Maximum duration of heatmap, in seconds (default: %(default)s)")
    parser.add_argument('--mtu', type=int, default=1500,
            help="All packets larger than the MTU will be ignored (default: %(default)s)")
    parser.add_argument('--x-resolution', '-x', type=float, default=0.1,
            help="Chunk size for x axis on heatmap (default: %(default)s)")
    parser.add_argument('--y-resolution', '-y', type=float, default=32,
            help="Chunk size for y axis on heatmap (default: %(default)s)")
    parser.add_argument('--write', '-w', default=None,
            help="Write results to file (default: nothing is written if this flag is omitted)")
    parser.add_argument('--verbose', '-v', action='count', default=0)
    return parser

def add_heatmap_args(parser):
    """
    Add up the arguments specific to producing a heatmap.

    :param parser: Argument parser with the common arguments
    :type parser: `py:ArgumentParser`
    :return: Argument parser with the common arguments
    :rtype: `py:ArgumentParser`
    """
    parser.add_argument('--show', action='store_true',
            help="Show the graph instead of writing it to a file")
    parser.add_argument('--scaling-factor', '-f', type=float, default=0.1,
            help=("Dial back the maximum on the scale to avoid blowout; e.g. "
                  "treat the top 10%% of values equally (default: %(default)s)"))
    return parser

def _add_common_args(parser):
    """
    Adds the arguments for both training and making predictions.

    :param parser: Argument parser with the common arguments
    :type parser: `py:ArgumentParser`
    :return: Argument parser with the common arguments
    :rtype: `py:ArgumentParser`
    """
    parser.add_argument('--hidden-layer-size', '-l', type=int, default=[], action="append",
            help=("The number of nodes in the hidden layers, can be used "
                  "multiple times for multiple hidden layers"))
    parser.add_argument('--test-confidence', '-c', type=float, default=0.5,
            help=("The percentage confidence in our prediction required to"
                  "positively classify a stream (default: %(default)s)"))
    return parser

def add_prediction_args(parser):
    """
    Adds the arguments for making predictions.

    :param parser: Argument parser with the common arguments
    :type parser: `py:ArgumentParser`
    :return: Argument parser with the common arguments
    :rtype: `py:ArgumentParser`
    """
    parser = _add_common_args(parser)
    parser.add_argument('--load', default=None, required=True,
            help="Load training weights from saved .ckpt file (default: None)")
    return parser

def add_training_args(parser):
    """
    Add up the arguments for training/testing neural nets.

    :param parser: Argument parser with the common arguments
    :type parser: `py:ArgumentParser`
    :return: Argument parser with the common arguments
    :rtype: `py:ArgumentParser`
    """
    parser = _add_common_args(parser)
    parser.add_argument('--load', default=None,
            help="Load training weights from saved .ckpt file (default: None)")
    parser.add_argument('--training-protocol', '-t', default="http",
            help="The type of protocol this training this is for (default: %(default)s)")
    parser.add_argument('--training-iterations', '-i', type=int, default=1000,
            help="The number of times to run the optimizer (default: %(default)s)")
    parser.add_argument('--test-percentage', '-p', type=float, default=0.2,
            help="Percentage of data that should be held back for testing (default: %(default)s)")
    return parser

def get_inputs(packets):
    """
    Processes a list of packets to extract the features we care about.

    :param packets: list of (time, packet) where time is a unix timestamp
                    and the packet is binary packet data (this is what the
                    pcap.readpkts() method returns).
    :type packets: list of tuples (int, bytes)
    :return: list of tuples containing (the packet size, elapsed time, and
             packet size of the next packet).
    :rtype: list of tuples (int, float, int)
    """
    t0 = packets[0][0]  # get the starting time so we can make things relative
    table = [(p[0]-t0, len(p[1])) for p in packets]  # [(timing,size), ...]
    output = []
    for t in range(1, len(table)):
        output.append((table[t-1][1], table[t][0]-table[t-1][0], table[t][1]))
    [debug(x) for x in output]
    return output

def split_stream(packets, src):
    """
    This takes a stream of packets and splits them into sender/receiver.

    :param packets: A list of packets to show a heatmap of
    :type packets: `py:PacketList`
    :param src: The MAC address to consiter to be the sender
    :type src: str
    :returns: List of sent packets and received packets
    :rtype: Tuple (List<Packets>, List<Packets>)
    """
    sent = []
    received = []
    for packet in packets:
        # divide packets into ones sent vs ones received
        if packet[Ether].src.startswith(src):
            sent.append(packet)
        else:
            received.append(packet)
    return sent, received

def aggrigate_heatmap_data(stream, y_limit, x_max, dx, dy, x, y, z):
    """
    This will aggrigate the data from the stream and append them to the x, y, z
    coordinates, creating those three variables if necessary.  If z is None, x
    and y will be ignored.

    :param stream: List of packets in the stream
    :type stream: List<packets>
    :param y_limit: the maximum packet size we will ever see in this dataset
    :type y_limit: int
    :param x_max: the maximum value for our x axis (in seconds)
    :type x_max: float
    :param dx: resolution of x access
    :type dx: float
    :param dy: resolution of y access
    :type dy: float
    :param x: 2D array defining X values
    :type x: 2D array of floats
    :param y: 2D array defining Y values
    :type y: 2D array of floats
    :param z: 2D array defining Z values
    :type z: 2D array of floats
    :returns: the updated x, y, and z values
    :rtype: tuple
    """
    if len(stream) < 1:
        return x, y, z

    t0 = stream[0].time
    src = stream[0][Ether].src
    if z is None:
        # We default to 1s instead of 0s to allow log scales to work properly
        z = ones((ceil(y_limit*2/dy), ceil(x_max/dx)))
        y, x = mgrid[slice(-y_limit, y_limit, dy),
                       slice(0, x_max, dx)]

    sent, received = split_stream(stream, src)
    for packet in sent:
        x_value = int(float(packet.time-t0)/dx)
        y_value = int((len(packet)+y_limit-dy)/dy)
        if x_value >= x_max/dx:
            continue
        debug("x_value={}, x_max={}, len(packet)={}, y_limit={}, y_value={}".format(
              x_value, x_max, len(packet), y_limit, y_value))
        z[y_value][x_value] += 1
    for packet in received:
        x_value = int(float(packet.time-t0)/dx)
        y_value = int((y_limit-len(packet)+dy)/dy)
        debug("processing packet received")
        if x_value >= x_max/dx:
            continue
        debug("x_value={}, x_max={}, len(packet)={}, y_limit={}, y_value={}".format(
              x_value, x_max, len(packet), y_limit, y_value))
        z[y_value][x_value] += 1

    return x, y, z

def load_model(name):
    """
    Loads the weights from a saved model and re-creates the neural net.

    :param name: Filename prefix to use when loading the weights
    :type name: str
    :returns: Tuple of (the neural network, output layer, optimizer, X
              placeholder, Y placeholder)
    :rtype: Tuple(`py:tensorflow.compat.v1.InteractiveSession`,
                  `py:tensorflow.Tensor`,
                  `py:tensorflow.compat.v1.train.AdamOptimizer`,
                  `py:tensorflow.compat.v1.placeholder`,
                  `py:tensorflow.compat.v1.placeholder`)
    """
    info("Loading data from {}".format(name))
    net = compat.v1.Session()
    saver = compat.v1.train.import_meta_graph('{}.meta'.format(name))
    saver.restore(net, name)
    X = net.graph.get_tensor_by_name("X:0")
    Y = net.graph.get_tensor_by_name("Y:0")
    output_layer = net.graph.get_tensor_by_name("output_layer:0")
    optimizer = net.graph.get_collection("optimizer")[0]
    return net, output_layer, optimizer, X, Y

def show_heatmap(x, y, z, scaling_factor, show_plot, output_filename):
    """
    Plots output in a heatmap, with the X axis being time and the Y access
    being packet sizes sent/recevied (sent being positive, received being
    negative.  Packets are assumed to be time-ordered.

    :param x: 2D array defining X values
    :type x: 2D array of floats
    :param y: 2D array defining Y values
    :type y: 2D array of floats
    :param z: 2D array defining Z values
    :type z: 2D array of floats
    :param show_plot: Flag to indicate whether to show the plot to the user
    :type show_plot: bool
    :param output_filename: The output filename to write out the heatmap to,
                            or None to not write anything out
    :type output_filename: str
    :returns: nothing
    :rtype: None
    """
    # Often times a few high values are outliers that throw off the scale
    # To deal with this, we treat the top N% of values as "maximum" in order
    # to provide more visibility into the other packets.  This makes it easier
    # to see what's going on in the graph, which is the entire point of a
    # visualization.
    max_scale = unique(z)[int(-(len(unique(z))*scaling_factor))]  # omit top N%
    pcolor(x, y, z, norm=LogNorm(vmin=z.min(), vmax=max_scale), cmap='inferno')
    colorbar()
    plt.ylabel("Received <- packet size -> Sent")
    plt.xlabel("time (s)")
    if show_plot:
        plt.show()
    if output_filename is not None:
        plt.savefig(output_filename)

def read_heatmap_data(session, x, y, z, args):
    """
    Takes in a list of packets that makes up a session and returns them as x
    and y values in list format.

    :param session: List of Packet data
    :type session: List<packets>
    :param x: 2D array defining X values
    :type x: 2D array of floats
    :param y: 2D array defining Y values
    :type y: 2D array of floats
    :param z: 2D array defining Z values
    :type z: 2D array of floats
    :param args: Arguments provided by the user
    :type args: `py:Namespace`
    :returns: tuple of 2D arrays defining X, Y and Z values
    :rtype x: (2D array of floats, 2D array of floats, 2D array of floats)
    """
    packets = [packet for packet in session if len(packet) <= args.mtu]
    debug("Processing packets for {}".format(session))
    x, y, z = aggrigate_heatmap_data(packets,
                                     args.mtu,
                                     args.max_duration,
                                     args.x_resolution,
                                     args.y_resolution,
                                     x, y, z)
    return x, y, z

def training_test_split(z, training_protocol, output_labels, test_percentage=0.2):
    """
    This takes the inputs from a labeled dataset and converts it into X and y
    values, and splits them into training and test validation sets.
    """
    X_train, y_train = [], []
    X_test, y_test = [], []

    z = z.reshape(-1)  # We should probably use MinMaxScaler(feature_range=(-1,1).fit(z)
    y_value = [1 if x == training_protocol else 0 for x in output_labels]
    if random() > test_percentage:
        X_train.append(list(z))
        y_train.append(list(y_value))
    else:
        X_test.append(list(z))
        y_test.append(list(y_value))
    return X_train, y_train, X_test, y_test

def get_bidirectional_sessions(pcap_file):
    """
    The default .sessions() thing only provides one-way streams, so each
    packetlist has the same sender for every packet.  This function will
    obtain the bidirectional streams.  This algorithm is simplistic and
    just groups them together based on the source and destination (including
    port).

    :param pcap_file: filename of the pcap to read and extract the sessions from
    :type pcap_file: str
    :returns: dictionary containing all of the bidirectional streams
    :rtype: Dictionary<str, `py:PacketList`>
    """
    sessions = rdpcap(pcap_file).sessions()
    sessions2 = {}
    for k in sessions.keys():
        parts = k.split(' ')
        rev = " ".join([parts[0], parts[3], parts[2], parts[1]])
        # First we see if we've already seen this in the reverse direction
        if rev in sessions2:
            sessions2[rev].res.extend(sessions[k].res)
            # PacketLists should always be in cronological order
            sessions2[rev].sort(key=lambda x: x.time)
        else:
            if k in sessions2:
                sessions2[k].res.extend(sessions[k].res)
            else:
                sessions2[k] = sessions[k]
            # PacketLists should always be in cronological order
            sessions2[k].sort(key=lambda x: x.time)
    return sessions2

def process_pcaps_heatmap(args):
    """
    Read in all the pcaps and put them into a heatmap matrix which can then be
    displayed as a heatmap, processed to determine what a partcular protocol
    looks like on average, or matched against known protocols.

    :param args: Arguments provided by the user
    :type args: `py:Namespace`
    :returns: tuple of 2D arrays defining X, Y and Z values
    :rtype x: (2D array of floats, 2D array of floats, 2D array of floats)
    """
    for pcap_file in args.pcap:
        debug("Reading in: {}".format(pcap_file))
        sessions = get_bidirectional_sessions(pcap_file)
        x, y, z = None, None, None
        for session in sessions:
            debug("Processing packets for {}".format(session))
            x, y, z = read_heatmap_data(sessions[session], x, y, z, args)
    return x, y, z

def convert_and_scale(values):
    """
    Takes a nested list of values and converts them into scaled, numpy arrays.

    :param values: Nested list of values (inputs or outputs)
    :type values: List<List<float|int>>
    :returns: Numpy array of those same values
    :rtype: `py:numpy.array`
    """
    scaler = MinMaxScaler(feature_range=(-1,1))
    values = asarray(values)
    scaler.fit(values)
    return values

def build_neural_net(n_features, n_outputs, hidden_layer_sizes):
    """
    This will build a neural network based on the given inputs.  It also links
    together the input, hidden layers, and outputs, initializes the weights,
    and returns everything that will be needed for training and predictions.

    :param n_features: the number of features that will be in each input
    :type n_features: int
    :param n_outputs: the number of nodes that will be in each output
    :type n_outputs: int
    :param hidden_layer_sizes: a list of the sizes of hidden layers, currently
                               only the first one is used, but this argument is
                               here to keep the API stable for when support is
                               added for multiple hidden layers
    :type hidden_layer_sizes: List<int>
    :returns: Tuple of (the neural network, output layer, cost, optimizer, X
              placeholder, Y placeholder)
    :rtype: Tuple(`py:tensorflow.compat.v1.InteractiveSession`,
                  `py:tensorflow.Tensor`, `py:tensorflow.Tensor`,
                  `py:tensorflow.compat.v1.train.AdamOptimizer`,
                  `py:tensorflow.compat.v1.placeholder`,
                  `py:tensorflow.compat.v1.placeholder`)
    """
    debug("Hidden layer sizes = {}".format(hidden_layer_sizes))
    n_hidden_layer_1 = hidden_layer_sizes[0]
    net = compat.v1.InteractiveSession()
    compat.v1.disable_eager_execution()
    X = compat.v1.placeholder(dtype=float32, shape=[None, n_features], name="X")
    Y = compat.v1.placeholder(dtype=float32, shape=[None, n_outputs], name="Y")

    # initialize the weights
    scaling_factor = 1
    weight_initializer = keras.initializers.VarianceScaling(mode="fan_avg",
                                                            distribution="uniform", scale=scaling_factor)
    bias_initializer = zeros_initializer()

    # First hidden layer (dimensions: n_features in, n_hidden_layer_1 out)
    W_hidden_1 = Variable(weight_initializer([n_features, n_hidden_layer_1]))
    bias_hidden_1 = Variable(bias_initializer([n_hidden_layer_1]))
    hidden_1 = nn.relu(add(matmul(X, W_hidden_1), bias_hidden_1))

    # Add middle layers (if any)
    n_previous_layer = n_hidden_layer_1
    previous_layer = hidden_1
    for n_hidden_layer in hidden_layer_sizes[1:]:
        debug("Creating hidden_layer with {} nodes".format(n_hidden_layer))
        # Weight for this hidden layer takes (nodes in previous layer, nodes in this layer)
        W_hidden_n = Variable(weight_initializer([n_previous_layer, n_hidden_layer]))
        # Bias for this hidden layer takes (nodes in this layer)
        bias_hidden_n = Variable(bias_initializer([n_hidden_layer]))
        # Create our layer using (the previous layer, our weights), and our bias
        hidden_n = nn.relu(add(matmul(previous_layer, W_hidden_n), bias_hidden_n))

        # Make the current layer now be the previous layer
        n_previous_layer = n_hidden_layer
        previous_layer = hidden_n

    # Output layer (dimensions: n_hidden_layer_n in, n_outputs out)
    W_out = Variable(weight_initializer([n_previous_layer, n_outputs]))
    bias_out = Variable(bias_initializer([1]))
    output_layer = add(matmul(previous_layer, W_out), bias_out, name="output_layer")

    # Cost & optimization
    cost = reduce_mean(math.squared_difference(output_layer, Y))
    optimizer = compat.v1.train.AdamOptimizer().minimize(cost)
    #LEARNING_RATE = 1e-4
    #optimizer = compat.v1.train.GradientDescentOptimizer(LEARNING_RATE).minimize(cost)
    compat.v1.add_to_collection("optimizer", optimizer)

    return net, output_layer, cost, optimizer, X, Y

def train_neural_net(net, X, Y, X_train, y_train, optimizer, epochs=1000, batch_size=100):
    """
    This function trains a neural net using the labeled samples passed in.  It
    is expected that the dimensions of the X and Y values will match the
    dimensions of the neural net.

    :param net: The neural net that we are putting to the test
    :type net: `py:tensorflow.compat.v1.InteractiveSession`
    :param X_test: 2D array defining X values, one input per row
    :type X_test: 2D array of floats
    :param y_test: 2D array defining Y values, one output per row
    :type y_test: 2D array of floats
    :param optimizer: The optimizer to use, which should already be configured
                      to minimize the cost
    :type optimizer: `py:tensorflow.compat.v1.train.AdamOptimizer` or
                     `py:tensorflow.compat.v1.train.GradientDescentOptimizer`
                     or any other optimizer from tensorflow
    :param epochs: The number of iterations to do in the optimizer
    :type epochs: int
    :param batch_size: The taining data will be split up into chunks to reduce
                       memory usage, this argument controls the size of those
                       chunks
    :type batch_size: int
    :returns: The neural net that we trained
    :rtype: `py:tensorflow.compat.v1.InteractiveSession`
    """
    info("Starting training process...")
    for e in range(epochs):
        shuffle_data = permutation(arange(len(y_train)))
        X_train = X_train[shuffle_data]
        y_train = y_train[shuffle_data]
        debug("Processing {} batches".format(len(y_train) // batch_size+1))
        for i in range(0, (len(y_train) // batch_size + 1)):
            start = i * batch_size
            batch_x = X_train[start:start + batch_size]
            batch_y = y_train[start:start + batch_size]
            net.run(optimizer, feed_dict={X: batch_x, Y: batch_y})
            debug("Batch {} done".format(i))
        debug("Epoch done")
    info("Training complete")
    return net

def test_neural_net(net, X, out, X_test, y_test, confidence_required=0.5):
    """
    This takes in a neural network, a set of inputs not used for training, and
    a set of outputs which are known to be correct, and determines the accuracy
    of the neural net.

    :param net: The neural net that we are putting to the test
    :type net: `py:tensorflow.compat.v1.InteractiveSession`
    :param X_test: 2D array defining X values, one input per row
    :type X_test: 2D array of floats
    :param y_test: 2D array defining Y values, one output per row
    :type y_test: 2D array of floats
    :param confidence_required: the level of certainty we need to output a "1"
                                in the output.  The default is .5 to provide a
                                "more likely than not" approach
    :type confidence_required: float
    :returns: accuracy as a percentage
    :rtype: float
    """
    info("Running test data...")
    pred = net.run(out, feed_dict={X: X_test})
    total = len(pred)
    correct = 0
    for i in range(0, total):  # for each predition...
        correct += 1  # mark it correct until we find out otherwise
        for j in range(0, len(pred[i])):
            # Check each output for this prediction
            if pred[i][j] - y_test[i][j] > 1-confidence_required:
                # if any output is off by more than acceptable error,
                # consider the entire answer to be incorrcet and move on
                correct -= 1
                break
    accuracy = correct/total
    info("Accuracy = {}/{} = {:.2f}%".format(correct, total, accuracy*100))
    return accuracy


if __name__ == "__main__":
    parser = set_args()
    parser = add_training_args(parser)

    args = parser.parse_args()
    # Start with CRITICAL and knock 10 off for each -v
    basicConfig(level=(CRITICAL-args.verbose*10), format=FORMAT)

    X_train, y_train = [], []
    X_test, y_test = [], []
    for pcap_file in args.pcap:
        debug("Reading in: {}".format(pcap_file))
        sessions = get_bidirectional_sessions(pcap_file)
        for session in sessions:
            x, y, z = read_heatmap_data(sessions[session], None, None, None, args)
            xtr, ytr, xte, yte = training_test_split(z, args.training_protocol,
                    OUTPUT_LABELS, args.test_percentage)
            X_train.extend(xtr)
            y_train.extend(ytr)
            X_test.extend(xte)
            y_test.extend(yte)

        # GREETZ https://blog.quantinsti.com/deep-learning-artificial-neural-network-tensorflow-python/
        X_train = convert_and_scale(X_train)
        y_train = convert_and_scale(y_train)
        X_test = convert_and_scale(X_test)
        y_test = convert_and_scale(y_test)

        # Build the NN
        hidden_layer_sizes = [128]
        if args.hidden_layer_size:
            hidden_layer_sizes = args.hidden_layer_size
        net, output_layer, cost, optimizer, X, Y = build_neural_net(X_train.shape[1],
                y_train.shape[1], hidden_layer_sizes)
        # Train the NN
        net = train_neural_net(net, X, Y, X_train, y_train, optimizer, args.training_iterations)
        # Test the NN
        test_neural_net(net, X, output_layer, X_test, y_test, args.test_confidence)
