#!/usr/bin/env python3
from argparse import ArgumentParser
from logging import basicConfig, debug, info, CRITICAL
from pcap_analyzer import add_training_args, build_neural_net, convert_and_scale,\
                          get_bidirectional_sessions, load_model,\
                          read_heatmap_data, set_args, test_neural_net,\
                          train_neural_net, training_test_split, FORMAT,\
                          OUTPUT_LABELS
try:
    from scapy.all import rdpcap, IP
    from tensorflow.compat.v1 import Session, global_variables_initializer
    from tensorflow.compat.v1.train import import_meta_graph, Saver
except ImportError as e:
    print("Error: {}; try: sudo apt install -y python3-pip python3-tk; pip3 install scapy matplotlib numpy".format(str(e)))
    from sys import exit; exit(1)



if __name__ == "__main__":
    parser = set_args()
    parser = add_training_args(parser)
    args = parser.parse_args()
    # Start with CRITICAL and knock 10 off for each -v
    basicConfig(level=(CRITICAL-args.verbose*10), format=FORMAT)

    X_train, y_train = [], []
    X_test, y_test = [], []
    for pcap_file in args.pcap:
        debug("Reading in: {}".format(pcap_file))
        sessions = get_bidirectional_sessions(pcap_file)
        for session in sessions:
            x, y, z = read_heatmap_data(sessions[session], None, None, None, args)
            xtr, ytr, xte, yte = training_test_split(z, args.training_protocol,
                    OUTPUT_LABELS, args.test_percentage)
            X_train.extend(xtr)
            y_train.extend(ytr)
            X_test.extend(xte)
            y_test.extend(yte)

        # GREETZ https://blog.quantinsti.com/deep-learning-artificial-neural-network-tensorflow-python/
        X_train = convert_and_scale(X_train)
        y_train = convert_and_scale(y_train)
        X_test = convert_and_scale(X_test)
        y_test = convert_and_scale(y_test)
        info("Data split into {} test and {} training ({:.2f}% for training)".format(len(X_test),
                len(X_train), len(X_test)/(len(X_train)+len(X_test))*100))

        if args.load:
            net, output_layer, optimizer, X, Y = load_model(args.load)
        else:
            # Build the NN
            hidden_layer_sizes = args.hidden_layer_size if args.hidden_layer_size else [128]
            net, output_layer, _, optimizer, X, Y = build_neural_net(X_train.shape[1],
                    y_train.shape[1], hidden_layer_sizes)
            global_variables_initializer().run()

        # Train the NN
        net = train_neural_net(net, X, Y, X_train, y_train, optimizer, args.training_iterations)
        if args.write:
            info("Saving neural network weights to {}".format(args.write))
            saver = Saver(max_to_keep=1)
            saver.save(net, args.write)

        # Test the NN
        accuracy = test_neural_net(net, X, output_layer, X_test, y_test, args.test_confidence)
        print("Accuracy is {:.2f}%".format(accuracy*100))
