#!/usr/bin/env python3

from argparse import ArgumentParser
from logging import basicConfig, debug, info, CRITICAL
from pcap_analyzer import set_args, add_heatmap_args, process_pcaps_heatmap,\
                          show_heatmap, FORMAT
try:
    from scapy.all import rdpcap, IP
except ImportError as e:
    print("Error: {}; try: sudo apt install -y python3-pip python3-tk; pip3 install scapy matplotlib numpy".format(str(e)))
    from sys import exit; exit(1)

if __name__ == "__main__":
    parser = set_args()
    parser = add_heatmap_args(parser)
    args = parser.parse_args()
    # Start with CRITICAL and knock 10 off for each -v
    basicConfig(level=(CRITICAL-args.verbose*10), format=FORMAT)

    x, y, z = process_pcaps_heatmap(args)
    show_heatmap(x, y, z, args.scaling_factor, args.show, args.write)
