# Cookbook
There are several things that this package does, creating heatmaps, training
the neural network, and making predictions.

# Heatmaps
Heatmaps are for a person to look at the data in a way that should make patterns
obvious.  The main reason one would look at a heatmap would be to see what an
average transaction of a particular type looks like.  For example, taking a
packet capture and then filtering only DNS requests and responses and then
running it through the heatmap would show you the timing and size of an average
DNS transaction.

Heatmaps are primarially useful for debugging the code.  If a human can tell
the difference between one type of traffic and another by looking at the
heatmaps, then Eye of Providence should be able to do the same.  If a human can
not tell the difference, then the system may not be able to figure it out.

To generate a heatmap of dns.pcap:
```
./generate_heatmap.py --show dns.pcap
```

While this will get you started, there are some important flags that you are
likely going to want to set.

For starters, --max-duration will control how much of the stream we're
analyzing.  This means anything to the right is going to be cut off.  If it
looks like the data is going off the screen, increase this value.  This
actually changes the number of packets being analyzed.

Next we have the --mtu which probably won't need to be modified, but it sets
the limit on the packet size that we'll accept for analysis.  Virtual networks
can have packets far beyond 1500 bytes (we've seen 40000+ byte packets in
practice), and those make the Y scale really tiny.  That is why the default is
set to 1500.

To control the resolution of the heatmap, we have --x-resolution (in seconds)
and --y-resolution (in bytes).  These values control how big a pixel is in the
heatmap.  If multiple packets came in between 0 and 0.1 seconds, all between
128-159 bytes, they would all fall into the same pixel by default.  If the Y
resolution were lowered, it would mean the pixels would be smaller and these
packets might get broken up into separate pixels.  Changing these values will
change the data that the system is working with, not just how it is displayed.
Increasing the resolution will cause increased memory usage, and going too high
will cause a problem if you don't have enough data.  Higher resolution without
enough data to fill it will mean each pixel will only have one or two packets,
which is less useful than clumping ones together that are reasonably similar.
Note: when we talk about "resolution" here, we're not referring to the size of
the image that is produced, but rather the granularity of a block on the graph.

If everything looks very dark, this means you likely have at least one pixel in
the heatmap which is far hotter than the rest.  We attempt to compensate for
this by using a logrithmic scale, but sometimes that's not enough.  For times
like these, the --scaling-factor will help.  This only affects the
visualization, and the higher the number, the more it will brighten up the dark
(but not empty) spots so they are more visible if there is something there.
Setting this to 1.0 makes the heatmap monotone, which is probably not what you
want, but it illustrates how it works.

If you want to write the heatmap out to a file instead of having it pop up in
an interactive manner, drop the --show option and add --write FILENAME

As you can see, the debugging that the heatmaps facilitate is not only helping
to find bugs in the code, but it helps you determine what parameters to use

# Training
Looking at heatmaps may be fun, but it doesn't enable the computer to predict
anything.  To do that, we need to train a neural network to know what a
particular type of data looks like so it can recognize it later.  The input to
the training process should all be the same type.  Unrelated packets can be
filtered out in wireshark with the desired packets exported to a pcap that is
fed into this program.

In its simpliest form, training looks like this:

```
./train_model.py --training-protocol dns dns.pcap
```

However, this is probably not what you actually want.  This will read in the
pcap, split the inputs into two (training and testing datasets), train the
neural net, test it to see how many it can predict correctly (using the test
data set), print out the accuracy and exit.  It doesn't save the training
data to be re-used later.

The --write FILENAME option is used to save the training results to a file.
This saves the theta values, which are not exactly tied to the training data,
but rather tied to the properties of a particular type of traffic.

The --training-protocol option is telling the system what type of data is being
fed into the machine.  If it's omited, it will default to http.  Due to the way
neural networks work, there must be a set of possible output nodes, which in
our case are protocols or activities.  Currently, these are hard-coded in the
OUTPUT_LABELS variable in pcap_analyzer.py.  This ensures that the output
labels used in training match those used in prediction.  At some point these
will be made more configurable, in which case it'll be up to the user to make
sure what was used for training matches what is used in predictions. This means
the same number of options and in the same order.  Change anything, and you'll
have to do your training all over again.

The --max-duration, --x-resolution, --y-resolution, and --mtu that were
described in the Heatmap section (above) apply to training as well.  These
values will affect how accurate the predictions will be.

The other things that impacts accuracy are the shape of the neural network, the
number of training iterations that are done, and the confidence required before
outputing a prediction.  Lets take these one at a time.

The shape of the neural net describes how many hidden layers there are, and how
many nodes are in each of these layers.  The specific trade offs are beyond the
scope of this tool, but are described in great detail in academic papers over
the past several decades.  The general idea is that more hidden layers and more
hidden nodes will likely mean higher accuracy, but at the cost of a higher
amount of effort required for both training and prediction.  The simplest way
to really understand these is to play around with them and see if your changes
are making things better or worse, and whether the amount of time it takes to
train is worth the benefit of accuracy (which is entirely subjective).  The
command line option is --hidden-layer-size and it can be specified more than
once to add additional hidden layers.  For example, this would train using
three hidden layers which are 512, 256, and 128 nodes, respectively:

```
./train_model.py --hidden-layer-size 512 --hidden-layer-size 256 \
                 --hidden-layer-size 128 --training-protocol dns dns.pcap
```

Next is the number of training iterations.  The process of training is an
attempt to optimize certain values.  The more you train, the more optimized
they should become.  However, there is a law of dimishing returns where an
extra 10% of training isn't going to get you an extra 10% of accuracy.  Maybe
it will at first, but then it will hardly get you anything.  The default
number of iterations seems to be reasonable, but you can change it with the
--training-iterations option.

Finally, if you want to change the amount of data which is held back for the
test set (to determine the accuracy of the training values), this can be
adjusted with --test-percentage.

# Predictions
TODO
