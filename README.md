# Eye of Providence
This tool analyzes network traffic to determine what protocol is being used,
which it should be able to do whether the traffic is wrapped in a TLS tunnel or
not. For example, it should be able to discerne betwen TLS protected HTTP,
IMAP, and SMTP streams. It does this based on the size and timing of packets.

# Usage
1. Capture network traffic into a pacp file
2. Run it through the analyzer (see [cookbook](cookbook.md))
3. The analyzer will output all streams and their protocols

# How it works
It takes the packet size, duration until the next packet in the stream, and
then the next packet size as a 3-part tuple. These serve as the inputs to the
analyzer.

The analyzer is a neural network that takes in an array of inputs and outputs
the probability that it is all of the protocols that it knows about.

The visualizer is a way of seeing the packets in a graphical format.

# Profile Heatmaps
Profile heatmaps show what an average stream looks like.  For example, here's
what a DNS request/response looks like:

<table><tr><th style="text-align: center;"><b>DNS</b></th></tr>
<tr><td width="740"><p align="right">
![dns](/images/dns.png "Short transactions, small requests, varied size responses")
</p></td></tr>
</table>

The X axis is time, starting with the moment the first packet of the stream was
seen.  The Y axis is packet size and it is split into two parts.  The top is
packets sent, and the bottom is packets received.  This graphic is made up of
many DNS requests/responses and the brighter the color, the more packets of
that size were seen at that time and in that direction (sending or receiving).
This is quite a different profile than what is seen in HTTPS.

<table><tr><th style="text-align: center;"><b>HTTPS</b></th></tr>
<tr><td width="740"><p align="right">
![https](/images/https.png "Long transactions, mostly small requests and responses but more variation")
</p></td></tr>
</table>

With HTTPS, we see that most of the requests are small, as are most of the
responses.  The streams also laste longer than DNS, extending out past that
half second mark most of the time.  There is a lot more variation in the
packet sizes in both directions, but the responses are more varied than the
requests.

Note: HTTPS was used over HTTP because it is difficult to find websites that
use HTTP and are more than just a redirect to the HTTPS version of the site.
If I can get a list of HTTP sites to browse, I'd love to do a comparison
between HTTP and HTTPS!

# Limitations
This system can only recognize traffic that it has been trained to recognize.

It will also have a very limited ability to glean much meaningful information
from any systems which specifically try to obfuscate packet length or timing.
For example, Tor sends its traffic in packets that are always the same size.
This means it is easy to detect that they are using Tor, but more difficult to
determine what protocol they are using underneath.

# Credits
This idea was directly ripped off of an academic researcher named [Charles V
Wright](https://web.archive.org/web/20161107231048/http://www.cs.jhu.edu/~cwright/traffic-viz/).
He deserves the credit
for the idea, I deserve credit for making this tool freely available. Because
Charles only released the idea, and not the code nor the data files, this tool
was written entirely from scratch without his knowledge nor consent.

